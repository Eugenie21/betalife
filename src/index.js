import './style/index.scss';
import $ from 'jquery';

function handleNavButtonClick() {
  const target = $(this).data('target');
  const $target = $(`#${target}`);

  if (!$target) {
    return;
  }

  $('#nav-menu-button').removeClass('is-open');
  $('#nav-menu').removeClass('show');

  $('html, body').animate({
    scrollTop: $target.offset().top
  }, 600);
}

function handleCaptchaSuccess($form, response) {

  const data = $form.serializeArray();

  if (data) {

    $form.find('input, textarea, button').attr('disabled', true);

    const sendOptions = {
      Host : "mail.betalife.tech",
      Username : "support@betalife.tech",
      Password : "1vaPAn1sa7",
      From : "support@betalife.tech",
      Subject : "This is the subject",
      Body : JSON.stringify(data)
    }

    Promise.all([
      Email.send({
        ...sendOptions,
        To: 'info@betalife.tech'
      }),
      Email.send({
        ...sendOptions,
        To: 'm663005549@gmail.com'
      }),
    ]).then(message => {
      $('#main-message').addClass('Message--show');
      $form.find('input, textarea').val('');
    }).finally(() => {
      $form.find('input, textarea, button').attr('disabled', false);
    });
  }
}

function handleFormSubmit(event) {
  event.preventDefault();
  const $form = $(event.currentTarget);

  const recaptchaSiteKey = '6LcRYPcUAAAAAB5qRDX6fl-F6nSYZSGSf4DrDfyf';

  if (grecaptcha) {
    grecaptcha.execute(recaptchaSiteKey, { action: "social" })
      .then(token => handleCaptchaSuccess($form, token));
  }
}

$(document).ready(() => {

  $('#nav-menu-button').click(function() {
    $(this).toggleClass('is-open');
    $('#nav-menu').toggleClass('show');
  });

  $('#nav-menu button').click(handleNavButtonClick);
  $('#down-button').click(handleNavButtonClick);
  $('#contacts-form').submit(handleFormSubmit);
  $('#main-message, #main-message .Message__CloseBtn, #main-message .Message__Button').click(() =>
    $('#main-message').removeClass('Message--show')
  );
});
