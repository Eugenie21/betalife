const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'build')
  },
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: true,
    port: 8080
  },
  resolve: {
    extensions: ['.js', '.scss', '.pug']
  },
  module: {
    rules: [{
      test: /\.s[ac]ss$/i,
      use: [
        'style-loader',
        {
          loader: 'css-loader',
          options: {
            url: false
          }
        },
        'sass-loader',
      ],
    }, {
      test: /\.(js)$/,
      exclude: /node_modules/,
      use: ['babel-loader']
    }, {
      test: /\.(pug)$/,
      use: ['pug-loader']
    }, {
      test: /\.(png|jpe?g|gif|svg)$/i,
      loader: 'url-loader'
    }],
  },
  plugins: [
    new HTMLWebpackPlugin({
      filename: 'index.html',
      template: './static/templates/index.pug',
      title: 'Станьте партнером Aviakassa'
    }),
    new CopyWebpackPlugin([
      {from: './static/images', to: 'images'},
      {from: './static/fonts', to: 'fonts'},
    ])
  ]
}
